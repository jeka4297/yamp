package easy_pack;

/**
 * Created by qbolt on 23.12.2016.
 */
public class Quadratic {
    public static Double[] solve(float a,float b,float c){
        Double arr[]=new Double[2];
        if(a==0)
            return arr;
        float des=b*b-4*a*c;
        if(des<0){
            return arr;
        }
        if(des==0){
            arr[0]=(double) ((-b)/(2*a));
            return arr;
        }
        arr[0]=(-b+Math.sqrt(des))/(2*a);
        arr[1]=(-b-Math.sqrt(des))/(2*a);
        return arr;
    }
}
