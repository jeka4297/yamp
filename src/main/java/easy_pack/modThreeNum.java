package easy_pack;

import java.util.Scanner;

/**
 * Created by qbolt on 23.12.2016.
 */
public class modThreeNum {

    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        System.out.println("Введите три числа ");
        float f[]=new float[3];
        f[0]=sc.nextFloat();
        f[1]=sc.nextFloat();
        f[2]=sc.nextFloat();
        System.out.println("Произведение = "+(f[0]*f[1]*f[2]));
        System.out.println("Среднее = "+((f[0]+f[1]+f[2])/3));
        sort(f);
        System.out.println(f[0]+" "+f[1]+" "+f[2]);
    }

    private static void sort(float f[]){
        for(int j=0;j<f.length;j++)
            for(int i=0;i<f.length-1;i++){
                if(f[i+1]<f[i]){
                    float temp=f[i];
                    f[i]=f[i+1];
                    f[i+1]=temp;
                }

            }
    }

}
