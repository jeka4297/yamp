package easy_pack;
import java.util.Scanner;

/**
 * Created by qbolt on 23.12.2016.
 */
public class LinEqu {

    public static void main(String[] args){
        //ax+bx=c
        //Ax+By=C
        Scanner in = new Scanner(System.in);

        System.out.println("{ax+by=c");
        System.out.println("{Ax+By=C");
        System.out.println("=========");


        double a=0;
        double A=0;

        double b=0;
        double B=0;

        double c=0;
        double C=0;

        double i=0;
        double j=0;
        double k=0;

        double x=0;
        double y=0;

        System.out.println("Enter the number a");
        a=in.nextDouble();
        System.out.println("Enter the number A");
        A=in.nextDouble();
        System.out.println("Enter the number b");
        b=in.nextDouble();
        System.out.println("Enter the number B");
        B=in.nextDouble();
        System.out.println("Enter the number c");
        c=in.nextDouble();
        System.out.println("Enter the number C");
        C=in.nextDouble();

        x=( ( (c*B)-(b*C)   ) / ( (a*B)-(b*A) ) );
        y=( ( (a*C)-(c*A) )   / ( (a*B)-(b*A) ) );

        System.out.println("{"+a+"x+ "+ b+"y= "+c );
        System.out.println("{"+A+"x+ "+ B+"y= "+C );
        System.out.println("x= "+x+ " y= "+ y);
//EnD.
    }
}
