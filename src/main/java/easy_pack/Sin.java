package easy_pack;

import java.util.Scanner;

/**
 * Created by qbolt on 23.12.2016.
 */
public class Sin {


    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        System.out.println("Please, write range");
        float left,right;
        left=sc.nextFloat();
        right=sc.nextFloat();

        System.out.println("Please, write step");
        float step=sc.nextFloat();
        for(;left<=right || Math.abs(left - right) < 1e-5;left+=step){
            System.out.println("sin("+left+")="+Math.sin(left));
        }
    }

}
