package easy_pack;

import java.util.Scanner;

/**
 * Created by qbolt on 23.12.2016.
 */
public class Korni {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Please, write three coefficients");
        float a,b,c;
        a=sc.nextFloat();
        b=sc.nextFloat();
        c=sc.nextFloat();
        Double arr[]=Quadratic.solve(a, b, c);
        if(arr[0]!=null)
            System.out.println("first x = "+arr[0]);
        else
            System.out.println("Empty");
        if(arr[1]!=null)
            System.out.println("Second x = "+arr[1]);
    }

}
