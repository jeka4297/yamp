package string_pack;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by qbolt on 16.12.2016.
 */
public class StringProcessor {

    /**
     *1. На входе строка s и целое число N.
     * Выход — строка, состоящая из N копий строки s, записанных подряд.
     * При N = 0 результат — пустая строка.
     * При N < 0 выбрасывается исключение.
     */
    void copy(String s, int N){

        try{
            if (N < 0) {
                System.out.print(" ");
            }
        } catch(ArithmeticException e){
            System.out.print("Ошибка. Второй аргумент должен быть больше или равен 0");
        }

        if( N == 0) {
            System.out.print(" ");
        } else
        for (int i = 0; i < N; i++) {
            System.out.print(s);
        }

    }


    /**
     * На входе две строки. Результат — количество вхождений второй строки в первую
     * @param s -- вводимая строка
     * @param in -- строка, которая находится в строке s.
     * @return
     */
    int subString(String s, String in){
        int count = 0;
        for(String chips: s.split(in)){
            count++;
        }
        return count;
    }



    void change(String s){
         // Я 1 и у меня 2 друга
        String w = s.replaceAll("1", "один");
        String ww = w.replaceAll("2", "два");
        String www = ww.replaceAll("3", "три");
        System.out.print(www);
    }


    void delSecond(StringBuilder s){

        for (int i = 0; i < s.length(); i++) {
            s.delete(i+1,i+2);
        }
        System.out.print(s);
    }

    public  static void main(String[] args){
        String S= " 1 2 3 1 1 1";
        StringBuilder Z = new StringBuilder("PaPaPaPa");
        StringProcessor obj= new StringProcessor();
        System.out.println( obj.subString(S,"1"));
        obj.change(S);
        System.out.println();
        obj.delSecond(Z);
   }

}
