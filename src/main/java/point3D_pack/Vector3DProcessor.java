package point3D_pack;

import point3D_pack.Vector3D;

/**
 * Created by qbolt .
 */
public class Vector3DProcessor {
    private static final Vector3D zero = new Vector3D(0,0,0);

    public static Vector3D sum(Vector3D a, Vector3D b){
        return new Vector3D(a.getX()+b.getX(),a.getY()+b.getY(),a.getZ()+b.getZ());
    }
    public static Vector3D substraction(Vector3D a, Vector3D b){
        return new Vector3D(a.getX()-b.getX(),a.getY()-b.getY(),a.getZ()-b.getZ());
    }

    public static double scalarComposition(Vector3D a, Vector3D b){
        return a.getX()*b.getX()+a.getY()*b.getY()+a.getZ()*b.getZ();
    }

    public static Vector3D vectorComposition(Vector3D a, Vector3D b){
        return new Vector3D(a.getY()*b.getZ()-a.getZ()*b.getY(),a.getZ()*b.getX()-a.getX()*b.getZ(),a.getX()*b.getY()-a.getY()*b.getX());
    }


    public static boolean collinearity(Vector3D a, Vector3D b){
        Vector3D c = vectorComposition(a,b);
        return c.equals(zero);
    }
}