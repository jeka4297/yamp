package point3D_pack;

import static java.lang.Math.abs;

/**
 * Created by qbolt on 08.11.2016.
 */
public class Point3D {

    private double x;
    private double y;
    private double z;
    private double eps = 0.0001;

    public Point3D() {}

    public Point3D(double a, double b, double c){

        x = a;
        y = b;
        z = c;
    }

    public double getX() {
        return x;
    }
    public void setX(double x) {
        this.x = x;
    }
    public double getY() {
        return y;
    }
    public void setY(double y) {
        this.y = y;
    }
    public double getZ() {
        return z;
    }
    public void setZ(double z) {
        this.z = z;
    }

    public void out(){
        System.out.print( " x: " + this.getX() + " y: " + this.getY() + " z: " + this.getZ() );
    }

    public boolean equals(Point3D p){
        return (abs(x - p.x) < eps) && (eps > abs(y - p.y)) && (abs(z - p.z) < eps);
    }

    public  static void main(String[] args){
        Point3D p = new Point3D();
        System.out.println("Coordinate point on X = " +p.getX());
        System.out.println("Coordinate point on Y = " +p.getY());
        System.out.println("Coordinate point on Z = " +p.getZ());
    }
}
