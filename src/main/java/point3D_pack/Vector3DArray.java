package point3D_pack;


/**
 * Created by qbolt
 */
public class Vector3DArray {

    private Vector3D[] arr;

    public Vector3DArray(int a) {
        arr = new Vector3D[a];
        for (int i = 0; i < a; i++) {
            arr[i] = new Vector3D();
        }
    }

    public double getLength() {
        return arr.length;
    }

    public void changeElement(Vector3D a, int b) {
        arr[b] = a;
    }

    public double maxLengthVector() {
        double a = 0;
        for (Vector3D anArr : arr) {
            if (anArr.length() > a) {
                a = anArr.length();
            }
        }
        return a;
    }

    public int find(Vector3D a) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(a)) {
                return i;
            }
        }
        return -1;
    }

    public Vector3D sum() {
        Vector3D c = new Vector3D();
        for (Vector3D anArr : arr) {
            c = Vector3DProcessor.sum(anArr, c);//добавить sum в вектор
        }
        return c;
    }

    public Vector3D linearCombination(double[] d) {
        Vector3D c = new Vector3D();
        if (arr.length != d.length) return c;
        for (int i = 0; i < d.length; i++) {
            c.setX(c.getX() + d[i] * arr[i].getX());
            c.setY(c.getY() + d[i] * arr[i].getY());
            c.setZ(c.getZ() + d[i] * arr[i].getZ());
        }
        return c;
    }

    public Point3D[] pointShift(Point3D a) {
        Point3D[] parr = new Point3D[arr.length];
        for (int i = 0; i < arr.length; i++) {
            parr[i] = new Point3D(a.getX() + arr[0].getX(), a.getY() + arr[0].getY(), a.getZ() + arr[0].getZ());
        }
        return parr;
    }

}
