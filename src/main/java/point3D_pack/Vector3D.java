package point3D_pack;

import static java.lang.Math.abs;

/**
 * Created by qbolt.
 */
public class Vector3D {

    private double x;
    private double y;
    private double z;
    private double eps = 0.000001;

    public Vector3D() {
    }
    public double getX(){return x;}
    public double getY(){return y;}
    public double getZ(){return z;}
    public double getEps(){return eps;}
    public void setX(double a){x=a;}
    public void setY(double a){y=a;}
    public void setZ(double a){z=a;}
    public void setEps(double a){eps=a;}

    public Vector3D(double a, double b, double c) {
        x = a;
        y = b;
        z = c;
    }

    public Vector3D(Point3D a, Point3D b) {
        x = b.getX()-a.getX();
        y = b.getY()-a.getY();
        z = b.getZ()-a.getZ();
    }

    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3D vector3D = (Vector3D) o;

        return (abs(x - vector3D.x) < eps) && (eps > abs(y - vector3D.y)) && (abs(z - vector3D.z) < eps);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
