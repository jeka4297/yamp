package tailor;


class TailorExp {

    public static double exp(double x, double e) {
        double stx = x;
        double fat = 1; //значение факториала в знаменателе
        double ex = 1; //первый элемент в разложении
        double i = 1; //счётчик
        while ((stx/fat)>=e) { //до тех пор, пока элемент в разложении не меньше точности
            ex=ex+stx/fat;
            i++;
            stx=stx*x;
            fat=fat*i;
        }
        return(ex);
    }

}

