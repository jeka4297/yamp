package array_obj_pack;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by qbolt on 23.12.2016.
 */
public class FinanceReport {

    private Payment[] payments;


    //конструкторы
    public FinanceReport(Payment[] arrayOfPayments) {
        this.payments = new Payment[payments.length];

        for (int i = 0; i < arrayOfPayments.length; i++) {
            this.payments[i] = new Payment(payments[i]);
        }

    }

    public FinanceReport(){

    }

    public Payment access(int i){
        return payments[i];
    }

    public void name(char a, OutputStream reader) throws IOException{
        DataOutputStream datareader = new DataOutputStream(reader);
        for (int i = 0; i < payments.length; i++)
            if (payments[i].getFIO().charAt(0) == a) {
                datareader.writeChars(payments[i].toString());
                datareader.flush();
            }
    }

    public void ChoosePayments(int pay, OutputStream reader) throws IOException {
        DataOutputStream datareader = new DataOutputStream(reader);
        for (int i = 0; i < payments.length; i++) {
            if (payments[i].getSumma() <= pay) {

                datareader.writeChars(payments[i].toString());
                datareader.flush();
            }
        }
    }



    public void inter(InputStream inputStream) throws IOException{
        Scanner scanner = new Scanner(inputStream);
        ArrayList<Payment> paymentArrayList = new ArrayList<Payment>();
        while (scanner.hasNext()){
            String name= scanner.next();
            int day = scanner.nextInt();
            int month = scanner.nextInt();
            int year = scanner.nextInt();
            int sum = scanner.nextInt();
            paymentArrayList.add(new Payment(name,day,month,year,sum));
        }
        payments = new Payment[paymentArrayList.size()];
        for (int i=0; i< paymentArrayList.size();i++){
            payments[i]=paymentArrayList.get(i);
        }
    }






}
