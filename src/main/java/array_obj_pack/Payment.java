package array_obj_pack;

/**
 * Created by qbolt on 23.12.2016.
 */
public class Payment {

    private String FIO;
    private int date_day;
    private int date_month;
    private int date_year;
    private int summa;

    public Payment(String FIO, int date_day, int date_month, int date_year, int summa) {

        this.FIO = FIO;
        this.date_day = date_day;
        this.date_month = date_month;
        this.date_year = date_year;
        this.summa = summa;
    }

    public Payment(Payment payment) {

    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public int getDate_day() {
        return date_day;
    }

    public void setDate_day(int date_day) {
        this.date_day = date_day;
    }

    public int getDate_month() {
        return date_month;
    }

    public void setDate_month(int date_month) {
        this.date_month = date_month;
    }

    public int getDate_year() {
        return date_year;
    }

    public void setDate_year(int date_year) {
        this.date_year = date_year;
    }

    public int getSumma() {
        return summa;
    }

    public void setSumma(int summa) {
        this.summa = summa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        if (date_day != payment.date_day) return false;
        if (date_month != payment.date_month) return false;
        if (date_year != payment.date_year) return false;
        if (summa != payment.summa) return false;
        return FIO != null ? FIO.equals(payment.FIO) : payment.FIO == null;

    }

    @Override
    public int hashCode() {
        int result = FIO != null ? FIO.hashCode() : 0;
        result = 31 * result + date_day;
        result = 31 * result + date_month;
        result = 31 * result + date_year;
        result = 31 * result + summa;
        return result;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "FIO='" + FIO + '\'' +
                ", date_day=" + date_day +
                ", date_month=" + date_month +
                ", date_year=" + date_year +
                ", summa=" + summa +
                '}';
    }

}
