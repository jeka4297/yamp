package array_pack;

import java.util.Scanner;

public class Array{

    /**
     * Вывод массива на консоль
     */
    void out(int array[]){
        for(int i : array){
            System.out.print(i+" ");
        }
        System.out.println();
    }

    /**
     * Заполнение массива вручную
     */
    void writeA(int array[]){
        Scanner sc=new Scanner(System.in);
        System.out.println("please, fill array");
        for(int i=0;i<array.length;i++){
            System.out.print("arr["+i+"]=");
            array[i]=sc.nextInt();
            System.out.println();
        }
    }

    /**
     * Сумма элементов
     */
    int sum(int array[]){
        int summa=0;
        for(int i : array){
            summa+=i;
        }
        return summa;
    }

    /**
     * Количество чётных элементов в массиве
     */
    int evenCount(int array[]){
        int col=0;
        for(int i : array){
            if(i%2==0)
                col++;
        }
        return col;
    }

    /**
     * Количество элементов в отрезке [a,b]
     */
    int otrezok(int array[], int a, int b){
        /*Scanner sc=new Scanner(System.in);
		System.out.println("please, write the A and B");
		System.out.print("A=");
		int a=sc.nextInt();
		System.out.print("B=");
		int b=sc.nextInt();
        */
        int col=0;
        for(int i : array){
            if(i>=a && i<=b)
                col++;
        }
        return col;
    }

    /**
     * Проверка: все ли элементы положительные
     */
    boolean isPositive(int array[]){
        for(int i : array){
            if(i<0)
                return false;
        }
        return true;
    }

    int[] reverse(int array[]){
        int temp[]=new int[array.length];
        int j=temp.length-1;

        for(int i : array)
            temp[j--]=i;

        return temp;
    }


    public  static void main(String[] args){
        Array object = new Array();

        int arr[]=new int[5];

        arr = new int[]{0, 1, 2, 3, 4};
        System.out.print(object.evenCount(arr));


        System.out.println(object.isPositive(arr));

        System.out.println(object.sum(arr));
        System.out.println(object.evenCount(arr));
        System.out.println(object.otrezok(arr,1,3));

        /*
        Array outArray=new Array();


        int arr[]=new int[5];
        System.out.println("size of Array="+arr.length);
        outArray.writeA(arr);//записываем каждый элемент массива сами

        System.out.println("Array:");
        outArray.out(arr);//печатает массив

        System.out.println("Sum of array="+outArray.evenCount(arr));
        System.out.println("Number of even numbers="+outArray.evenCount(arr));//кол-во четных чисел в массиве

        System.out.println("please, write the A and B");
        Scanner sc=new Scanner(System.in);
        System.out.print("A=");
        int a=sc.nextInt();
        System.out.print("B=");
        int b=sc.nextInt();
        System.out.println("Number of numbers between A and B = "+outArray.otrezok(arr,a,b));//кол-во чисел в [A;B]

        System.out.println("Is array posotive = "+outArray.isPositive(arr));//Все ли эти элементы положительны

        System.out.println("Reverse array:");
        outArray.out(outArray.reverse(arr));//печатает массив задом наперед (с конца)*/
    }
}
