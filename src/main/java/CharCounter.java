import org.omg.PortableInterceptor.ACTIVE;

/**
 * Created by qbolt on 17.12.2016.
 */
public class CharCounter {
    //split
    /**
     * input: строка
     * output: число, равное количеству слов, содержащее три заглавные латинские буквы А
     *
     * @param s -- ввидоимая пользователем строка.
     *          count  -- количество слов подходящих по условию.
     *          miniCount -- количество букв А.
     */
    int word(String s) {
        int count = 0;
        int miniCount = 0;


        for (char chips : s.toCharArray()) {

            if (chips == 'A') {
                miniCount++;
            }

            if (chips == ' ') {
                if (miniCount == 3) {
                    count++;
                    miniCount = 0;
                } else {
                    miniCount = 0;
                }
            }

        }

        return count;
    }

}
