package tailor;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TestTailor {
    @Test
    public void simpleTest() {
        assertEquals(TailorExp.exp(2, 0.001) , Math.exp(2), 0.02);
    }
}