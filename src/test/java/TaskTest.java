import org.junit.Test;
import static  org.junit.Assert.*;

import static junit.framework.TestCase.fail;

/**
 * Created by qbolt on 17.12.2016.
 */
public class TaskTest {


    @Test
    public void just(){
        CharCounter obj = new CharCounter();
        String s= "AAA aaa AAA aaa AAA ";
        int result = obj.word(s);
        assertEquals(3, result);
    }

    @Test
    public void russian(){
        CharCounter obj = new CharCounter();
        //Первые латиницей, вторые кириллицей
        String s= "AAA ААА ";
        int result = obj.word(s);
        assertEquals(1, result);

    }

    @Test
    public void actual(){
        CharCounter obj = new CharCounter();
        String s= "AA Aaa AAA ";
        int result = obj.word(s);
        assertEquals(1, result);
    }

    @Test
    public void empty(){
        CharCounter obj = new CharCounter();
        String s= " ";
        int result = obj.word(s);
        assertEquals( 0 , result);
    }

    @Test
    public void poly(){
        CharCounter obj = new CharCounter();
        String s= "AAAA AAA AAAA ";
        int result = obj.word(s);
        assertEquals(1, result);
    }

    @Test
    public void text(){
        CharCounter obj = new CharCounter();
        String s= "HELLO MADAMA KAK YOUR MAMAMA ";
        int result = obj.word(s);
        assertEquals(2, result);
    }

}
